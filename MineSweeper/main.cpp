#include <Windows.h>
#include <ctime>
#include "game.h"

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    srand(time(NULL));
    return Game(hInstance).Run();
}
