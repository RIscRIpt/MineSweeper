//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by resources.rc
//
#define IDW_BOOM_SOUND                  6
#define IDD_DIALOG1                     101
#define IDD_MAINWINDOW                  101
#define IDR_MAIN_MENU                   103
#define IDD_MINES_BAR                   103
#define IDB_SPRITE                      104
#define IDD_DIALOGBAR1                  104
#define IDD_TIME_BAR                    104
#define IDD_NEWGAME                     105
#define IDI_ICON_MINE                   107
#define IDI_ICON_TIME                   114
#define IDW_WIN_SOUND                   118
#define IDC_TIME                        1002
#define IDC_TIME_IMG                    1003
#define IDC_MINES_IMG                   1004
#define IDC_MINES                       1005
#define IDC_FIELD                       1008
#define IDC_DIFF_EASY                   1009
#define IDC_DIFF_MEDIUM                 1010
#define IDC_DIFF_HARD                   1011
#define IDC_DIFF_CUSTOM                 1012
#define IDC_GROUP_DIFFICULTY            1013
#define IDC_COLUMNS                     1014
#define IDC_ROWS                        1015
#define IDC_LABEL_COLUMNS               1016
#define IDC_LABEL_ROWS                  1017
#define IDC_NEWGAME_OK                  1019
#define IDC_NEWGAME_CANCEL              1020
#define IDC_GROUP_MULTIPLAYER           1021
#define IDC_PLAYERS                     1022
#define IDC_LABEL_PLAYERS               1023
#define IDC_LABEL_MINES                 1024
#define IDC_STATUS_BAR                  1025
#define ID_GAME_NEWGAME                 40001
#define ID_WINDOW                       40002
#define ID_WINDOW_OPTIMALSIZE           40003
#define ID_GAME_RETRY                   40004
#define ID_GAME_CHEATMODE               40005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        119
#define _APS_NEXT_COMMAND_VALUE         40006
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
