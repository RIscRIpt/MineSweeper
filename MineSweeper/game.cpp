#include <Windows.h>
#include <windowsx.h>
#include <functional>

#include "game.h"
#include "drawing.h"
#include "resource.h"

static INT_PTR CALLBACK mainDialogProcStatic(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    Game *game;
    if(uMsg == WM_INITDIALOG) {
        SetWindowLongPtr(hwndDlg, DWLP_USER, lParam);
        game = (Game*)lParam;
    } else {
        game = (Game*)GetWindowLongPtr(hwndDlg, DWLP_USER);
    }
    return game->DialogProc(hwndDlg, uMsg, wParam, lParam);
}

static INT_PTR CALLBACK newGameDialogProcStatic(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    Game *game;
    if(uMsg == WM_INITDIALOG) {
        SetWindowLongPtr(hwndDlg, DWLP_USER, lParam);
        game = (Game*)lParam;
    } else {
        game = (Game*)GetWindowLongPtr(hwndDlg, DWLP_USER);
    }
    return game->NewGameDialogProc(hwndDlg, uMsg, wParam, lParam);
}

Game::Game(HINSTANCE hInstance) :
    hInstance(hInstance),
    hMainWindow(NULL),
    field(nullptr),
    cheatMode(false)
{
    createMainWindow();
    SetPlayers(1);
    makeField(9, 9, 10);
    optimizeWindowSize();
}

Game::~Game() {
    if(hMainWindow != NULL)
        DestroyWindow(hMainWindow);

    if(field != nullptr)
        delete field;
    
    Drawing::Dispose();
}

void Game::createMainWindow() {
    if(CreateDialogParam(hInstance, MAKEINTRESOURCE(IDD_MAINWINDOW), NULL, mainDialogProcStatic, (LPARAM)this) == NULL)
        Error(0xC7E2, L"Failed to Create Main Window!");
}

void Game::makeField(int rows, int columns, int mines) {
    if(field != nullptr)
        delete field;
    field = new Field(rows, columns, mines);
    field->OnStart(std::bind(&Game::Start, this));
    field->OnEnd(std::bind(&Game::End, this));
    if(cheatMode)
        field->CheatMode(true);
    updateWindowLayout();
    updateMinesRemaining();
    updateTimeElapsed();
    nPlayerTurn = 0;
    if(players > 1) {
        SetStatus(L"Player #%d turn", nPlayerTurn + 1);
    } else {
        SetStatus(nullptr);
    }
}

void Game::Error(int code, const wchar_t *szErrFmt, ...) {
    wchar_t buffer[1024];
    va_list va;
    va_start(va, szErrFmt);
    vswprintf(buffer, ARRAYSIZE(buffer), szErrFmt, va);
    va_end(va);
    MessageBox(hMainWindow, buffer, L"Fatal Error!", MB_ICONERROR);
    ExitProcess(code);
}

void Game::Start() {
    End();
    nTickTimer = SetTimer(hMainWindow, 0, 1000, NULL);
}

void Game::End() {
    if(nTickTimer != NULL) {
        KillTimer(hMainWindow, nTickTimer);
        nTickTimer = NULL;
    }
}

void Game::SetPlayers(int n) {
    players = n;
    if(players <= 1) {
        SetStatus(nullptr);
    }
}

void Game::SetStatus(wchar_t * fmt, ...) {
    if(fmt != nullptr) {
        wchar_t buffer[1024];
        va_list va;
        va_start(va, fmt);
        vswprintf(buffer, ARRAYSIZE(buffer), fmt, va);
        va_end(va);
        SetWindowText(hStatusBar, buffer);
        ShowWindow(hStatusBar, SW_SHOW);
    } else {
        ShowWindow(hStatusBar, SW_HIDE);
    }
}

int Game::Run() {
    MSG msg;
    while(GetMessage(&msg, NULL, 0, 0) != 0) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return msg.wParam;
}

INT_PTR Game::DialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch(uMsg) {
        case WM_PAINT:
            draw();
            break;

        case WM_SIZE:
        case WM_SIZING:
            updateWindowLayout();
            break;

        case WM_LBUTTONUP:
            openCell(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
            break;
        
        case WM_MBUTTONUP:
            openCellsAround(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
            break;
        
        case WM_RBUTTONUP:
            flagCell(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
            break;
        
        case WM_COMMAND:
            if(HIWORD(wParam) == 0)
                menuItemSelected(LOWORD(wParam));
            break;
        
        case WM_TIMER:
            updateTimeElapsed();
            break;

        case WM_GETMINMAXINFO:
            setWindowMinMaxSize((MINMAXINFO*)lParam);
            break;

        case WM_INITDIALOG:
            hMainWindow = hwndDlg;
            initMainWindow();
            break;

        case WM_CLOSE:
            DestroyWindow(hMainWindow);
            break;

        case WM_DESTROY:
            Drawing::Dispose();
            PostQuitMessage(0);
            break;

        default:
            return FALSE;
    }
    return TRUE;
}

INT_PTR Game::NewGameDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch(uMsg) {
        case WM_COMMAND:
            if(HIWORD(wParam) == 0) {
                switch(LOWORD(wParam)) {
                    case IDC_NEWGAME_OK:
                        if(!newGameSetup(hwndDlg))
                            break;

                    case IDC_NEWGAME_CANCEL:
                        EndDialog(hwndDlg, 0);
                        break;
                    
                    case IDC_DIFF_EASY:
                    case IDC_DIFF_MEDIUM:
                    case IDC_DIFF_HARD:
                    case IDC_DIFF_CUSTOM:
                        newGameSwitchDifficulty(hwndDlg, LOWORD(wParam));
                        break;

                    default:
                        break;
                }
            }
            break;
        
        case WM_INITDIALOG:
            PostMessage(GetDlgItem(hwndDlg, IDC_ROWS), EM_SETLIMITTEXT, 3, 0);
            PostMessage(GetDlgItem(hwndDlg, IDC_COLUMNS), EM_SETLIMITTEXT, 3, 0);
            PostMessage(GetDlgItem(hwndDlg, IDC_MINES), EM_SETLIMITTEXT, 3, 0);
            PostMessage(GetDlgItem(hwndDlg, IDC_PLAYERS), EM_SETLIMITTEXT, 1, 0);
            SetDlgItemText(hwndDlg, IDC_PLAYERS, L"1");
            break;

        case WM_CLOSE:
            EndDialog(hwndDlg, 0);
            break;

        default:
            return FALSE;
    }
    return TRUE;
}

void Game::menuItemSelected(WORD item) {
    switch(item) {
        case ID_GAME_RETRY:
            makeField(field->rows, field->columns, field->mines);
            break;

        case ID_GAME_NEWGAME:
            newGame();
            break;
        
        case ID_GAME_CHEATMODE:
            toggleCheatMode();
            break;

        case ID_WINDOW_OPTIMALSIZE:
            optimizeWindowSize();
            break;

        default:
            break;
    }
}

void Game::newGame() {
    DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_NEWGAME), hMainWindow, newGameDialogProcStatic, (LPARAM)this);
}

bool Game::newGameSetup(HWND hWndDlg) {
    wchar_t wcsRows[4], wcsCols[4], wcsMines[4], wcsPlayers[2];
    GetDlgItemText(hWndDlg, IDC_ROWS, wcsRows, ARRAYSIZE(wcsRows));
    GetDlgItemText(hWndDlg, IDC_COLUMNS, wcsCols, ARRAYSIZE(wcsCols));
    GetDlgItemText(hWndDlg, IDC_MINES, wcsMines, ARRAYSIZE(wcsMines));
    GetDlgItemText(hWndDlg, IDC_PLAYERS, wcsPlayers, ARRAYSIZE(wcsPlayers));

    int newRows = _wtoi(wcsRows);
    int newCols = _wtoi(wcsCols);
    int newMines = _wtoi(wcsMines);
    int newPlayers = _wtoi(wcsPlayers);

    if(newRows <= 0 || newRows > maxRows ||
       newCols <= 0 || newCols > maxCols ||
       newMines <= 0 || newMines > maxMines ||
       newPlayers <= 0 || newPlayers > maxPlayers)
        return false;
    
    SetPlayers(newPlayers);
    makeField(newRows, newCols, newMines);
    optimizeWindowSize();
    return true;
}

void Game::newGameSwitchDifficulty(HWND hWndDlg, WORD button) {
    EnableWindow(GetDlgItem(hWndDlg, IDC_ROWS), button == IDC_DIFF_CUSTOM);
    EnableWindow(GetDlgItem(hWndDlg, IDC_COLUMNS), button == IDC_DIFF_CUSTOM);
    EnableWindow(GetDlgItem(hWndDlg, IDC_MINES), button == IDC_DIFF_CUSTOM);

    int rows, cols, mines = -1;
    switch(button) {
        case IDC_DIFF_EASY:
            rows = 9;
            cols = 9;
            mines = 10;
            break;

        case IDC_DIFF_MEDIUM:
            rows = 16;
            cols = 16;
            mines = 40;
            break;

        case IDC_DIFF_HARD:
            rows = 16;
            cols = 30;
            mines = 99;
            break;
    }

    if(mines != -1) {
        wchar_t buffer[8];

        _itow_s(rows, buffer, ARRAYSIZE(buffer), 10);
        SetDlgItemText(hWndDlg, IDC_ROWS, buffer);

        _itow_s(cols, buffer, ARRAYSIZE(buffer), 10);
        SetDlgItemText(hWndDlg, IDC_COLUMNS, buffer);

        _itow_s(mines, buffer, ARRAYSIZE(buffer), 10);
        SetDlgItemText(hWndDlg, IDC_MINES, buffer);
    }
}

void Game::toggleCheatMode() {
    if(field == nullptr)
        return;
    
    MENUITEMINFO mii = { 0 };
    mii.cbSize = sizeof(mii);
    mii.fMask = MIIM_STATE;
    GetMenuItemInfo(hMainMenu, ID_GAME_CHEATMODE, FALSE, &mii);
    if(mii.fState & MFS_CHECKED) {
        //If checked, disable cheat mode
        field->CheatMode(false);
        mii.fState &= ~MFS_CHECKED;
        cheatMode = false;
    } else {
        //Otherwise, enable
        field->CheatMode(true);
        mii.fState |= MFS_CHECKED;
        cheatMode = true;
    }
    SetMenuItemInfo(hMainMenu, ID_GAME_CHEATMODE, FALSE, &mii);

    InvalidateRect(hMainWindow, NULL, TRUE);
}

void Game::initMainWindow() {
    if((hIcon = (HICON)LoadImage(hInstance, MAKEINTRESOURCE(IDI_ICON_MINE), IMAGE_ICON, GetSystemMetrics(SM_CXICON), GetSystemMetrics(SM_CYICON), 0)) == NULL)
        Error(0x1131, L"Failed to load mine icon");

    if((hSmIcon = (HICON)LoadImage(hInstance, MAKEINTRESOURCE(IDI_ICON_MINE), IMAGE_ICON, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), 0)) == NULL)
        Error(0x1531, L"Failed to load small mine icon");

    SendMessage(hMainWindow, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
    SendMessage(hMainWindow, WM_SETICON, ICON_SMALL, (LPARAM)hSmIcon);

    if(DWORD err = Drawing::Init(hInstance))
        Error(0xD7A1, L"Failed to Initialize Drawing! (error: %08x)", err);

    if((hMinesBar = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_MINES_BAR), hMainWindow, NULL)) == NULL)
        Error(0xC7EE, L"Failed to Create Mines Bar");

    if((hTimeBar = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_TIME_BAR), hMainWindow, NULL)) == NULL)
        Error(0xC7EE, L"Failed to Create Time Bar");
    
    hStatusBar = GetDlgItem(hMainWindow, IDC_STATUS_BAR);
    hMainMenu = GetMenu(hMainWindow);

    GetClientRect(hMinesBar, &rcMines);
    GetClientRect(hTimeBar, &rcTime);
    GetClientRect(hStatusBar, &rcStatus);
    barHeight = max(rcMines.bottom, rcTime.bottom);
}

void Game::optimizeWindowSize() {
    if(field != nullptr) {
        RECT rc;
        field->GetOptimalSize(rc);
        
        rc.bottom += barHeight;

        AdjustWindowRectEx(
            &rc,
            GetWindowLongPtr(hMainWindow, GWL_STYLE),
            TRUE,
            GetWindowLongPtr(hMainWindow, GWL_EXSTYLE));

        int width = rc.right - rc.left;
        int height = rc.bottom - rc.top;

        MoveWindow(
            hMainWindow,
            (GetSystemMetrics(SM_CXSCREEN) - width) / 2,
            (GetSystemMetrics(SM_CYSCREEN) - height) / 2,
            width,
            height,
            TRUE);

        //updateWindowLayout(); //should be called by the WM_SIZE message due to MoveWindow
    }
}


void Game::updateWindowLayout() {
    RECT rcMain;

    GetClientRect(hMainWindow, &rcMain);
    SetWindowPos(
        hMinesBar,
        0,
        0,
        0,
        0,
        0,
        SWP_NOSIZE | SWP_NOZORDER);
    
    SetWindowPos(
        hStatusBar,
        0,
        rcMines.right,
        0,
        rcMain.right - rcTime.right /*+ rcTime.left*/ - rcMines.right,
        barHeight,
        SWP_NOZORDER);

    SetWindowPos(
        hTimeBar,
        0,
        rcMain.right - rcTime.right /* + rcTime.left*/,
        0,
        0,
        0,
        SWP_NOSIZE | SWP_NOZORDER);

    if(field != nullptr) {
        field->SetRect(RECT{
            0,
            barHeight,
            rcMain.right - rcMain.left,
            rcMain.bottom - rcMain.top,
        });
    }

    InvalidateRect(hMainWindow, NULL, TRUE);
}

void Game::setWindowMinMaxSize(MINMAXINFO *mmi) {
    RECT rc = { 0 };
    //if(field != nullptr) {
    //    field->GetOptimalSize(rc);
    //}
    rc.right = max(minimalWindowWidth, rc.right);
    rc.bottom = max(minimalWindowHeight, rc.bottom);

    AdjustWindowRectEx(
        &rc,
        GetWindowLongPtr(hMainWindow, GWL_STYLE),
        TRUE,
        GetWindowLongPtr(hMainWindow, GWL_EXSTYLE));

    mmi->ptMinTrackSize.x = rc.right;
    mmi->ptMinTrackSize.y = rc.bottom;
}

void Game::draw() {
    if(field != nullptr) {
        if(!Drawing::Begin(hMainWindow))
            Error(0xD7AB, L"Failed to Begin Drawing");

        field->DrawCells();

        Drawing::End();
    }
}

void Game::openCell(LONG x, LONG y) {
    if(field != nullptr && !field->HasFinished()) {
        if(!field->Open(POINT{ x, y }))
            return;

        checkGameStatus();
        InvalidateRect(hMainWindow, NULL, FALSE);
    }
}

void Game::openCellsAround(LONG x, LONG y) {
    if(players > 1) {
        return;
    }
    if(field != nullptr && !field->HasFinished()) {
        if(!field->OpenAround(POINT{ x, y }))
            return;

        checkGameStatus();
        InvalidateRect(hMainWindow, NULL, FALSE);
    }
}

void Game::flagCell(LONG x, LONG y) {
    if(players > 1) {
        return;
    }
    if(field != nullptr && !field->HasFinished()) {
        field->ToggleFlag(POINT{ x, y });
        updateMinesRemaining();
        InvalidateRect(hMainWindow, NULL, FALSE);
    }
}

void Game::checkGameStatus() {
    if(field->HasExploded()) {
        field->OpenAll();
        PlaySound(MAKEINTRESOURCE(IDW_BOOM_SOUND), hInstance, SND_RESOURCE | SND_ASYNC);
        if(players <= 1) {
            SetStatus(L"You Lost!");
        } else {
            SetStatus(L"Player #%d lost!", nPlayerTurn + 1);
        }
    } else if(field->HasWon()) {
        PlaySound(MAKEINTRESOURCE(IDW_WIN_SOUND), hInstance, SND_RESOURCE | SND_ASYNC);
        if(players <= 1) {
            SetStatus(L"You Win!");
        } else {
            SetStatus(L"It's a tie!");
        }
    }
    if(field->HasFinished()) {
        updateMinesRemaining();
        updateTimeElapsed();
    } else {
        if(players > 1) {
            nPlayerTurn = (nPlayerTurn + 1) % players;
            SetStatus(L"Player #%d turn", nPlayerTurn + 1);
        }
    }
}

void Game::updateMinesRemaining() {
    wchar_t buffer[8];
    _itow_s(field->GetMinesRemaining(), buffer, ARRAYSIZE(buffer), 10);
    SetDlgItemText(hMinesBar, IDC_MINES, buffer);
}

void Game::updateTimeElapsed() {
    wchar_t buffer[16]; //00:00:00\0
    int timeElapsed = field->GetElapsedTime();
    swprintf(buffer, ARRAYSIZE(buffer), L"%02d:%02d:%02d",
             (timeElapsed / 3600) % 24,
             (timeElapsed / 60) % 60,
             (timeElapsed) % 60);
    SetDlgItemText(hTimeBar, IDC_TIME, buffer);
}
