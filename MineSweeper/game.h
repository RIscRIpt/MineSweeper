#pragma once

#include <Windows.h>

#include "field.h"

class Game {
public:
    Game(HINSTANCE hInstance);
    ~Game();

    int Run();

    INT_PTR CALLBACK DialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
    INT_PTR CALLBACK NewGameDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

    void Error(int code, const wchar_t *szErrFmt, ...);

    // Event handlers / should be called by the Field
    void Start();
    void End();

    void SetPlayers(int n);
    void SetStatus(wchar_t *fmt, ...);

private:
    void createMainWindow();
    void makeField(int rows, int columns, int mines);
    void initMainWindow();
    void updateWindowLayout();
    void setWindowMinMaxSize(MINMAXINFO *mmi);
    void draw();

    void menuItemSelected(WORD item);
    void newGame();
    bool newGameSetup(HWND hWndDlg);
    void newGameSwitchDifficulty(HWND hWndDlg, WORD button);
    void toggleCheatMode();
    void optimizeWindowSize();

    void openCell(LONG x, LONG y);
    void openCellsAround(LONG x, LONG y);
    void flagCell(LONG x, LONG y);

    void checkGameStatus();

    void updateMinesRemaining();
    void updateTimeElapsed();

    int players;
    int nPlayerTurn;
    bool cheatMode;

    HINSTANCE hInstance;
    UINT_PTR nTickTimer;
    HWND hMainWindow, hMinesBar, hTimeBar, hStatusBar;
    HICON hIcon, hSmIcon;
    HMENU hMainMenu;
    RECT rcMines, rcTime, rcStatus;
    LONG barHeight;

    Field *field;

    const int maxRows = 999;
    const int maxCols = 999;
    const int maxMines = 9999;
    const int maxPlayers = 9;

    const int minimalWindowWidth = 400;
    const int minimalWindowHeight = 400;
};