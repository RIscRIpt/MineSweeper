#include "drawing.h"
#include "resource.h"

HGDIOBJ Drawing::m_hOldObj = NULL;
HBITMAP Drawing::m_hCellSprite = NULL;
HWND Drawing::m_hWnd = NULL;
HDC Drawing::m_hDC = NULL;
HDC Drawing::m_hMemDC = NULL;
PAINTSTRUCT Drawing::m_ps = { 0 };

const LONG Drawing::cellSpriteCoords[] = {
    #define X(type, x) x,
    #include "drawing_sprite_def.h"
};

DWORD Drawing::Init(HINSTANCE hInstance) {
    if((m_hCellSprite = (HBITMAP)LoadImage(hInstance, MAKEINTRESOURCE(IDB_SPRITE), IMAGE_BITMAP, 0, 0, LR_DEFAULTCOLOR)) == NULL)
        return GetLastError();
    return 0;
}

void Drawing::Dispose() {
    if(m_hCellSprite != NULL)
        DeleteObject(m_hCellSprite);

    EndCells();
    End();
}

bool Drawing::Begin(const HWND hWnd) {
    m_hWnd = hWnd;

    if((m_hDC = BeginPaint(m_hWnd, &m_ps)) == NULL)
        return false;

    if((m_hMemDC = CreateCompatibleDC(m_hDC)) == NULL)
        return false;

    return true;
}

void Drawing::End() {
    if(m_hMemDC != NULL) {
        DeleteDC(m_hMemDC);
        m_hMemDC = NULL;
    }
    if(m_hWnd != NULL) {
        EndPaint(m_hWnd, &m_ps);
        m_hWnd = NULL;
    }
}

bool Drawing::BeginCells() {
    if((m_hOldObj = SelectObject(m_hMemDC, m_hCellSprite)) == NULL)
        return false;
    return true;
}

void Drawing::EndCells() {
    if(m_hMemDC != NULL) {
        SelectObject(m_hMemDC, m_hOldObj);
        m_hOldObj = NULL;
    }
}

void Drawing::DrawCell(CellType type, const RECT &rc) {
    StretchBlt(
        m_hDC,
        rc.left,
        rc.top,
        rc.right - rc.left,
        rc.bottom - rc.top,
        m_hMemDC,
        cellSpriteCoords[type],
        0,
        cellSpriteWidth,
        cellSpriteHeight,
        SRCCOPY);
}
