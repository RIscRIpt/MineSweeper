#pragma once

#include <Windows.h>
#include <functional>

#include "cell.h"

class Field {
public:
    Field(int rows, int columns, int mines);
    ~Field();

    inline void OnStart(std::function<void()> fn) { onStart = fn; }
    inline void OnEnd(std::function<void()> fn) { onEnd = fn; }

    bool Open(const POINT &p);
    bool OpenAround(const POINT &p);
    void ToggleFlag(const POINT &p);
    void OpenAll();
    void Finished();
    void CheatMode(bool enable);

    void SetRect(const RECT &rc);
    void GetOptimalSize(RECT &rc);
    void DrawCells();

    inline bool HasStarted() { return startTime != -1; }
    inline bool HasExploded() { return exploded; }
    inline bool HasWon() { return !HasExploded() && nClosedCells == mines; }
    inline bool HasFinished() { return HasExploded() || HasWon(); }
    int GetMinesRemaining();
    int GetElapsedTime();

    const int rows, columns, mines;

private:
    void open(Cell &c);

    Cell* getCell(const POINT &p);
    Cell*& getCell(const int row, const int column);
    void iterateAround(const Cell &c, std::function<void(Cell &c)> fIter);
    int countAround(const Cell &c, std::function<bool(const Cell &c)> fCounter);
    int minesAround(const Cell &c);
    int flagsAround(const Cell &c);

    void updateCellsRects();
    void calcCellSize(LONG *width, LONG *height);

    std::function<void()> onStart;
    std::function<void()> onEnd;

    Cell **cells;
    RECT rect;

    bool exploded;
    int flaggedMines;
    int startTime, endTime;
    int nClosedCells;

    const int optimalCellWidth = 16;
    const int optimalCellHeight = 16;
};