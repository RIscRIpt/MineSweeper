#include <ctime>

#include "field.h"
#include "drawing.h"

Field::Field(int rows, int columns, int mines) :
    rows(rows),
    columns(columns),
    mines(mines),
    flaggedMines(0),
    startTime(-1),
    endTime(-1),
    exploded(false),
    nClosedCells(rows * columns)
{
    cells = new Cell*[rows * columns];
    for(int row = 0; row < rows; row++)
        for(int column = 0; column < columns; column++)
            getCell(row, column) = new Cell(row, column);
        
    mines = min(mines, rows * columns);
    while(mines--) {
        int row, col;
        do {
            row = rand() % rows;
            col = rand() % columns;
        } while(getCell(row, col)->IsMine());
        getCell(row, col)->SetMine(true);
    }
}

Field::~Field() {
    if(cells != nullptr) {
        for(int row = 0; row < rows; row++)
            for(int column = 0; column < columns; column++)
                delete getCell(row, column);
        delete[] cells;
    }
}

void Field::SetRect(const RECT &rc) {
    if(rect.left != rc.left
       || rect.top != rc.top
       || rect.right != rc.right
       || rect.bottom != rc.bottom)
    {
        rect = rc;
        updateCellsRects();
    }
}

void Field::GetOptimalSize(RECT & rc) {
    rc.top = 0;
    rc.left = 0;
    rc.right = optimalCellWidth * columns;
    rc.bottom = optimalCellHeight * rows;
}

void Field::updateCellsRects() {
    LONG cellWidth, cellHeight;
    calcCellSize(&cellWidth, &cellHeight);
    for(int row = 0; row < rows; row++)
        for(int column = 0; column < columns; column++)
            getCell(row, column)->SetRect(RECT{
                rect.left + column * cellWidth,
                rect.top + row * cellHeight,
                rect.left + column * cellWidth + cellWidth,
                rect.top + row * cellHeight + cellHeight,
            });
}

void Field::calcCellSize(LONG *width, LONG *height) {
    *width = (rect.right - rect.left) / columns;
    *height = (rect.bottom - rect.top) / rows;
}

void Field::DrawCells() {
    Drawing::BeginCells();
    for(int i = 0; i < rows * columns; i++)
        cells[i]->Draw();
    Drawing::EndCells();
}

int Field::GetMinesRemaining() {
    return mines - flaggedMines;
}

int Field::GetElapsedTime() {
    if(HasFinished())
        return endTime - startTime;
    else if(HasStarted())
        return time(NULL) - startTime;
    else
        return 0;
}

void Field::open(Cell &c) {
    if(!c.IsOpened() && !c.IsFlagged()) {
        int ma = minesAround(c);
        c.Open(ma);
        nClosedCells--;
        if(c.IsMine() && !HasExploded()) {
            exploded = true;
            c.SetMineInitial(true);
        }
        if(ma == 0) {
            iterateAround(c, std::bind(&Field::open, this, std::placeholders::_1));
        }
    }
}

bool Field::Open(const POINT &p) {
    Cell *c = getCell(p);
    if(c != nullptr && !c->IsFlagged() && !c->IsOpened()) {
        if(!HasStarted()) {
            startTime = time(NULL);
            if(onStart != nullptr)
                onStart();
        }
        open(*c);
        if(HasFinished())
            Finished();
        return true;
    } else {
        return false;
    }
}

bool Field::OpenAround(const POINT &p) {
    Cell *c = getCell(p);
    if(c != nullptr && c->IsOpened()) {
        if(flagsAround(*c) == c->GetMinesAround()) {
            iterateAround(*c, std::bind(&Field::open, this, std::placeholders::_1));
            if(HasFinished())
                Finished();
            return true;
        }
    }
    return false;
}

void Field::ToggleFlag(const POINT &p) {
    Cell *c = getCell(p);
    if(c != nullptr && !c->IsOpened()) {
        c->ToggleFlag();
        flaggedMines += c->IsFlagged() ? +1 : -1;
    }
}

void Field::OpenAll() {
    for(int i = 0; i < rows * columns; i++)
        cells[i]->Open(minesAround(*cells[i]));
}

void Field::Finished() {
    endTime = time(NULL);
    flaggedMines = exploded ? 0 : mines;
    if(onEnd != nullptr)
        onEnd();
}

void Field::CheatMode(bool enable) {
    for(int i = 0; i < rows * columns; i++)
        cells[i]->SetTransparent(enable);
}

Cell* Field::getCell(const POINT &p) {
    LONG cellWidth, cellHeight;
    calcCellSize(&cellWidth, &cellHeight);
    int row = (p.y - rect.top) / cellHeight;
    int col = (p.x - rect.left) / cellWidth;
    if(row < 0 || col < 0 || row >= rows || col >= columns)
        return nullptr;
    return getCell(row, col);
}

Cell*& Field::getCell(const int row, const int column) {
    return cells[row * columns + column];
}

void Field::iterateAround(const Cell & c, std::function<void(Cell&c)> fIter) {
    int baseR =       c.row == 0            ? 0 : -1;
    int baseC =    c.column == 0 			? 0 : -1;
    int endR  =       c.row == rows - 1 	? 0 : +1;
    int endC  =    c.column == columns - 1  ? 0 : +1;
    for(int row = baseR; row <= endR; row++)
        for(int column = baseC; column <= endC; column++)
            if(row != 0 || column != 0)
                fIter(*getCell(c.row + row, c.column + column));
}

int Field::countAround(const Cell &c, std::function<bool(const Cell&c)> fCounter) {
    int count = 0;
    iterateAround(c, [&count, &fCounter](Cell &c) {
        count += fCounter(c);
    });
    return count;
}

int Field::minesAround(const Cell &c) {
    return countAround(c, [](const Cell &c) { return c.IsMine(); });
}

int Field::flagsAround(const Cell &c) {
    return countAround(c, [](const Cell &c) { return !c.IsOpened() && c.IsFlagged(); });
}
