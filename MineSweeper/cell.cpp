#include "cell.h"
#include "drawing.h"

Cell::Cell(int row, int column) :
    row(row),
    column(column),
    isMine(false),
    isMineInitial(false),
    isOpened(false),
    isFlagged(false),
    isTransparent(false),
    nMinesAround(0)
{ }

Cell::~Cell() {

}

void Cell::SetRect(const RECT &rc) {
    rect = rc;
}

void Cell::Draw() {
    Drawing::CellType type;
    if(isOpened) {
        if(isMineInitial) {
            type = Drawing::OPENED_MINE_INIT;
        } else if(isMine) {
            type = isFlagged ? Drawing::CLOSED_FLAGGED : Drawing::OPENED_MINE;
        } else {
            type = isFlagged ? Drawing::OPENED_MINE_WRONG : (Drawing::CellType)nMinesAround;
        }
    } else {
        if(isTransparent && !isFlagged)
            type = isMine ? Drawing::CLOSED_MINE : Drawing::CLOSED;
        else
            type = isFlagged ? Drawing::CLOSED_FLAGGED : Drawing::CLOSED;
    }
    Drawing::DrawCell(type, rect);
}

void Cell::Open(int minesAround) {
    if(!isOpened) {
        isOpened = true;
        nMinesAround = minesAround;
    }
}

void Cell::ToggleFlag() {
    if(!isOpened)
        isFlagged = !isFlagged;
}
