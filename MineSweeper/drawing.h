#pragma once

#include <Windows.h>

class Drawing {
public:
    enum CellType {
        #define X(type, x) type,
        #include "drawing_sprite_def.h"
    };

    static DWORD Init(HINSTANCE hInstance);
    static void Dispose();

    static bool Begin(const HWND hWnd);
    static void End();

    static bool BeginCells();
    static void EndCells();

    static void DrawCell(CellType type, const RECT &rc);

private:
    static HGDIOBJ m_hOldObj;
    static HBITMAP m_hCellSprite;

    static HWND m_hWnd;
    static HDC m_hDC, m_hMemDC;
    static PAINTSTRUCT m_ps;

    static const int cellSpriteWidth = 128;
    static const int cellSpriteHeight = 128;
    static const LONG cellSpriteCoords[];
};
