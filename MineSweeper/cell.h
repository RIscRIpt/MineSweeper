#pragma once

#include <Windows.h>

class Cell {
public:
    Cell(int row, int column);
    ~Cell();

    void SetRect(const RECT &rc);
    void Draw();

    void Open(int minesAround);
    void ToggleFlag();

    inline void SetMine(bool mine) { isMine = mine; }
    inline void SetMineInitial(bool init) { isMine = true; isMineInitial = init; }
    inline void SetTransparent(bool transparent) { isTransparent = transparent; }

    inline bool IsMine() const { return isMine; }
    inline bool IsOpened() const { return isOpened; }
    inline bool IsFlagged() const { return isFlagged; }
    inline bool IsTransparent() const { return isTransparent; }
    inline int GetMinesAround() const { return nMinesAround; }

    const int row, column;

private:
    bool isMine;
    bool isMineInitial;
    bool isOpened;
    bool isFlagged;
    bool isTransparent;
    int nMinesAround;

    RECT rect;
};